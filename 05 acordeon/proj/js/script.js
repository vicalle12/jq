var action = "click";
var speed = 500;

$(document).ready(function(){
	$('li.q').on (action, function(){
		$(this).next().slideToggle(speed).siblings('li.a').slideUp();

		var img_actu = $(this).children('img');
		$('img').not(img_actu).removeClass('rotate');
		img_actu.toggleClass('rotate');
	});
});